" remap navigation keys
noremap æ l
noremap l k
noremap k j
noremap j h
" un-map 'h' key
nnoremap h <NOP>
" navigate pane's
" Use ctrl-[jklæ] to select the active split!
nnoremap <C-w>æ <C-w>l
nnoremap <C-w>l <C-w>k
nnoremap <C-w>k <C-w>j
nnoremap <C-w>j <C-w>h
nnoremap <C-w>h <NOP>
" move split panes to left/bottom/top/right
nnoremap <C-W>Æ <C-W>L
nnoremap <C-W>L <C-W>K
nnoremap <C-W>K <C-W>J
nnoremap <C-W>J <C-W>H
nnoremap <C-W>H <NOP>

syntax on
filetype plugin indent on
set number
set title
set showmatch
set linebreak
set wrap
set visualbell
set autoindent
set noexpandtab
set tabstop=4
set shiftwidth=4

if has("patch-7.4.710")
	:set listchars=eol:⏎,tab:>.,trail:~,extends:>,precedes:<
	",space:_
else
	:set listchars=eol:⏎,tab:>-,trail:·,extends:>,precedes:<
endif
:set list
